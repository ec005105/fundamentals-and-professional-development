# Introduction
The purpose of this report was to explore the requirements gathering and analysis stage of the systems development life cycle. A 1995 study of 365 businesses by the Standish Group found that 13.1% of projects were ultimately cancelled due to incomplete requirements with this being the largest single cause of project failure as well as being the second most common cause of challenge during a project that was later completed (1). Because of this, we must explore and document the requirements of our client to pave the way for successful execution and delivery of the software. 

![Causes of Project Impairement (Standish Group, 1995)](standishgraph.png)

This report is regarding the creation of an educational video game targeted towards university students. our client was a teacher who was interested in an educational "video" game with the main function of encouraging their university students to do their set readings. The information to be included is on the history of the Atlantic, which will include topics such as pirates and the slave trade. This task is to outline targets, requirements, and clarifications to help form a future business proposal.

# 1. Who (stakeholders) should you be asking about the requirements?
The teacher would be relied on to provide the key approval of ideas and outline of how they feel best their students learn. As they are the person approving the finished product and receiving the product in exchange for money, communication between the team and the teacher must be maintained throughout the development process. This is especially critical due to the broad scope of what they would like to be produced. Concepts and ideas will vary as there is no set direction other than the concept of a video game to get students to do their set readings.

Another key stakeholder is the students in question. While the teacher is vital in the general approval of ideas and concepts throughout the project, the students are the target audience and so (assuming the teacher hasn't already outlined students’ key demands) communication with students should also be attempted mainly through the form of data collection methods such as surveys to establish their requirements. As this will involve two separate groups of people there will need to be a relay of information from the students onto the teachers to ensure the teacher's goals with the product match those of the demands of students and if not to be able to reach a form of compromise to ensure the best possible outcome for the two groups. The data required from the student group would also need to include information regarding the target platforms with information like operating systems being needed to ensure compatibility with as high a percentage of target students as possible.

Communication with the IT systems at the university is an area that must be satisfied. Without communication with the IT systems team at the university, we cannot establish potential compatibility problems with the target audience. Communication topics would include distribution methods for the software (appsanywhere etc), any compatibility issues with pre-existing software or hardware within the university and any other potential problems regarding the use of our software at the university.

# 2. What key points do you need to clarify?

1. Time/deadline-  
 
This is how long the project is expected to be, or if there is a specific deadline the game should be made by.

2. Cost/Budget-
 
Determines level of attention to features throughout the project and how advanced the software will be. This is factored in with time and vice versa.

3. Hardware/Software the audience are using-

Enables the team to ensure compatibility with as many potential target devices as possible. Also, identify minimum specifications.

4. Accessibility features-

determine appropriate accessibility features based on student personal requirements.

5. Multiplayer or single player-

Find out what level of Multiplayer or single player gameplay they are after. Online multiplayer/single player? Offline multiplayer/single player? Clarification on what the teacher had in mind when referencing these.

6. Game genre-

determining appropriate genre to sensibly & effectively present the history of the Atlantic. Applying content from the set readings effectively.

7. Audience desired features-

Collecting and appropriately applying desired features (if necessary) outlined by the students.

8. Open-source vs Closed source-

Do we allow the software to be open to the public for their adaptation or use?

# 3 What constraints might there be?

1. Copyright material from the reading list

The customer has stated that they would like to encourage the students to complete the set readings lists. However, there is a potential issue with the software referencing the aforementioned books that can cause problems with copyright laws, particularly intellectual property. Understanding these laws is essential as copyright laws can be confusing. An example would be that there are exceptions to copyright under education and whether or not if the material has "minor uses" and would not "undermine sales of teaching materials" (2).

2. Legal issues with data (collection, storage and sharing)

Another important legal constraint could be the data protection of the students playing the game and the school. Will students sign up for the game with their school email addresses? If so, what system will be in place to protect the student’s information? Any stored private data needs to be dealt with under the general data protection regulation (3)

3. Ethical issues

Ethical issues surround the presentation of history as fact. Assuming the teacher was to at least provide sources that are deemed reputable otherwise issues involving checking facts are objective and credible would come up, not only costing the team time for fact credibility checking but also causing increased costs involving the project when sourcing material. Ensuring history is depicted sensibly and appropriately will be required due to the sensitive nature of topics such as the slave trade.

4. Costs vs Budget

Ensuring a balance between project costs and the budget outlined by the teacher is essential to the successful development and delivery of the project. Careful consideration needs to be taken when regarding factors such as work hours, costs relating to sourcing historical material, costs involved with game asset production (sourcing artwork, audio etc) and costs of any equipment used throughout production. Consideration of these factors can prevent budget restraints from becoming a larger issue.

5. Technical issues regarding target platforms

Issues can arise from creating software, in particular games, that target multiple different systems. Data regarding the system specifications of the target audience must be collected to get the software running on the lowest common denominator. These issues can be factored in a wider sense to include things like, if it was determined much of the target audience uses laptops without number pads, then care should be taken to ensure that the game does not have a reliance on the number pad.

# 4 Which points need clarification first?

The two single most important points are timeframe and budget, equally weighted. This is because without any idea of budget or time it becomes near impossible to rule out or, on the other hand, approve of a section within the project as there would be no indication as to how much time is available to spend on said section; and likewise, how much money can be dedicated to not only paying the wages of staff members allocated to the task but also for any software needed. Time and budget need to be balanced to find the optimum method for development, especially when developing video games of any type. An example of where this consideration needs to be taken is when laying the foundation of the game. Do you use a proprietary engine, like Unity or Unreal, that would decrease development time, however, increase costs for licenses and decrease overall customizability of the engine, or does the teamwork towards an in-house engine that, while taking longer for initial development, can directly target the desired audience and be designed to directly support desired mechanics (4)?

The game genre is an important point to be considered as it also ties in with budget, timeframe, and hardware/software requirements. As the client appears to still be unsure about the direction of the proposed "educational game", it would most likely be necessary to provide clear examples (5) of educational games and how they can tie in with encouraging students to complete the set readings. Examples would most likely include educational games such as the 1971 video game The Oregon Trail (6) which is described as a "choose-your-own-adventure strategy and hunt-to-survive gameplay"(7) created as an "educational tool" that ended up being used in "thousands of classrooms across the country [USA]" (7) used to teach about the emigrant trail of the same name. Alternatively, how more modern video gaming can be used to teach history like that of the Assassins Creed Odyssey discovery tour mode that combines concepts like the briefs "1st person adventure game" and quizzes as an educational tool that can be quoted as providing "joyful journeys into useful knowledge." (8). Presenting relevant examples like these to both our client (in this case the teacher) and target audience (relating to students) we would be able to uncover the more focused, less vague goal the teacher has in mind. This would vastly assist with cost and time predictions.


#references

(1) Standish Group (1995), Chaos. https://www.standishgroup.com/visitor/chaos.htm retrived from https://web.archive.org/web/20010603101314/https://www.standishgroup.com/visitor/chaos.htm snapshot on (2001, June 3) 

(2) Gov.uk (n.d), Exceptions to copyright: Education and Teaching, GOV.UK, https://assets.publishing.service.gov.uk/government/uploads/system/uploads/attachment_data/file/375951/Education_and_Teaching.pdf

(3) Gov.uk (n.d), Data Protection, GOV.UK, https://www.gov.uk/data-protection 

(4) T.Glaiel (2021, November 11), How to make your own game engine (and why), Medium Geek Culture, https://medium.com/geekculture/how-to-make-your-own-game-engine-and-why-ddf0acbc5f3

(5) S.Miller (n.d),How to Deal With a Vague Client, Small Buisness Chron, https://smallbusiness.chron.com/deal-vague-client-32008.html

(6) R.P Bouchard (2017, June 29), How I Managed to Design the Most Successful Educational Computer Game of All Time, Medium, https://medium.com/the-philipendium/how-i-managed-to-design-the-most-successful-educational-computer-game-of-all-time-4626ea09e184

(7) Fitzpatrick et al (2016, August 23), The 50 Best Video Games of All Time, TIME magazine, https://time.com/4458554/best-video-games-all-time/

(8) C. Campbell (2019, September 10), Assassin’s Creed Odyssey’s Discovery Tour is an inspiring journey through ancient Greece, Polygon, https://www.polygon.com/reviews/2019/9/10/20859403/assassins-creed-odysseys-discovery-tour-review-ancient-greece-education-game


